define(

    function module( ) {

        'use strict' ;

        var Astar ;

        Astar = function Astar( heuristic ) {

            var distance ;
            var getNode ;
            var initialize ;
            var neighbors ;
            var nodes ;
            var openHeap ;
            var setNode ;
            var updateHeap ;
            var walkable ;
            var world ;

            Astar.prototype.getPath = function getPath( grid, start, goal ) {

                var currentHeapIndex ;
                var currentNeighbor ;
                var currentNeighborIndex ;
                var currentNeighborweight ;
                var currentNeighbors ;
                var currentNode ;
                var estimateCost ;
                var goalNode ;
                var path ;
                var startNode ;

                world = {

                    'grid': grid,
                    'height': grid.length,
                    'width': grid[ 0 ].length
                } ;

                // define array of nodes to return when goal is fount
                path = [ ] ;

                // define nodes list
                nodes = new Array( world.width * world.height ) ;

                startNode = setNode( null, start ) ;
                goalNode = setNode( null, goal ) ;

                // add start and goal nodes to nodes list
                nodes[ start[ 0 ] + world.width * start[ 1 ] ] = startNode ;
                nodes[ goal[ 0 ] + world.width * goal[ 1 ] ] = goalNode ;

                startNode.heuristic = distance( startNode.position, goalNode.position ) ;

                // define open nodes heap descending ordered by sum of node cost and node heuristic
                openHeap = [ startNode ] ;

                startNode.opened = true ;

                // while open nodes heap contains any node then check the last node
                while ( openHeap.length > 0 ) {

                    currentNode = openHeap.pop( ) ;

                    // close current node
                    currentNode.closed = true ;
                    currentNode.opened = false ;

                    // if current node is the goal node then return constructed path
                    if ( currentNode === goalNode ) {

                        // while current node has a parent then construct path to the goal
                        while ( currentNode.parent !== null ) {

                            // add current node to the path
                            path.unshift( [ currentNode.position[ 0 ], currentNode.position[ 1 ] ] ) ;

                            // define current node for next iteration
                            currentNode = currentNode.parent ;
                        }

                        // add start node to the path
                        path.unshift( [ startNode.position[ 0 ], startNode.position[ 1 ] ] ) ;

                        // return constructed path from start to the goal
                        return ( path ) ;
                    }

                    currentNeighbors = neighbors( currentNode.position ) ;

                    // treat each current node's neighbor
                    while ( currentNeighbors.length > 0 ) {

                        // define current neighbor randomly
                        currentNeighborIndex = Math.floor( Math.random( ) * currentNeighbors.length ) ;
                        currentNeighbor = currentNeighbors[ currentNeighborIndex ] ;

                        // remove current neighbor from current node's neighbors
                        currentNeighbors.splice( currentNeighborIndex, 1 ) ;

                        // define current neighbor's weight
                        currentNeighborweight = world.grid[ currentNeighbor.position[ 1 ] ][ currentNeighbor.position[ 0 ] ] ;

                        // define current neighbor's estimate cost
                        estimateCost = currentNode.cost + distance( currentNode.position, currentNeighbor.position ) * currentNeighborweight ;

                        // if current neighbor is not opened yet then open it
                        if ( currentNeighbor.opened === false ) {

                            // add current neighbors to nodes list
                            nodes[ currentNeighbor.position[ 0 ] + world.width * currentNeighbor.position[ 1 ] ] = currentNeighbor ;

                            currentNeighbor.cost = estimateCost ;
                            currentNeighbor.heuristic = distance( currentNeighbor.position, goalNode.position ) ;
                            currentNeighbor.parent = currentNode ;

                            // add current neighbor to open nodes heap
                            updateHeap( currentNeighbor ) ;

                            currentNeighbor.opened = true ;
                        }

                        // if current neighbor is already opened then check if current node is a shorter parent to reach from start
                        else if ( estimateCost < currentNeighbor.cost ) {

                            // define current neighbor's index in open nodes heap
                            currentHeapIndex = openHeap.indexOf( currentNeighbor ) ;

                            // remove current neighbor from open nodes heap to update its index
                            openHeap.splice( currentHeapIndex, 1 ) ;

                            currentNeighbor.cost = estimateCost ;
                            currentNeighbor.parent = currentNode ;

                            // add current neighbor to open nodes heap to update its index
                            updateHeap( currentNeighbor ) ;
                        }
                    }
                }

                // return empty path
                return ( path ) ;
            } ;

            getNode = function getNode( position ) {

                var nodeIndex ;

                // define nodes list's whished index
                nodeIndex = position[ 0 ] + world.width * position[ 1 ] ;

                // return whished node if exists otherwise return a new node
                return ( nodes[ nodeIndex ] || setNode( null, position ) ) ;
            } ;

            initialize = function initialize( heuristic ) {

                // define distance function with user's heuristic
                distance = heuristic ;
            } ;

            neighbors = function neighbors( position ) {

                var diagonals ;
                var index ;
                var length ;
                var neighbor ;
                var neighbors ;
                var orthogonals ;
                var x ;
                var y ;

                x = position[ 0 ] ;
                y = position[ 1 ] ;

                diagonals = [ ] ;
                neighbors = [ ] ;

                // define top, left, right, bottom neighbors' positions
                orthogonals = [

                    [ x, y - 1 ],
                    [ x - 1, y ],
                    [ x + 1, y ],
                    [ x, y + 1 ]
                ] ;

                // find all unclosed walkable orthogonal neighbors
                for ( index = 0, length = orthogonals.length ; index < length ; index += 1 ) {

                    // if neighbor is walkable and is not closed then store it
                    if ( walkable( orthogonals[ index ] ) === true ) {

                        neighbor = getNode( orthogonals[ index ] ) ;

                        // if neighbor is not closed then store it
                        if ( neighbor.closed === false ) {

                            neighbors.push( neighbor ) ;
                        }
                    }
                }

                // define top-left, top-right, bottom-left, bottom-right neighbors' positions
                diagonals = [

                    [ x - 1, y - 1 ],
                    [ x + 1, y - 1 ],
                    [ x - 1, y + 1 ],
                    [ x + 1, y + 1 ]
                ] ;

                // find all unclosed walkable diagonal neighbors
                for ( index = 0, length = diagonals.length ; index < length ; index += 1 ) {

                    // if neighbor is walkable and is not closed then store it
                    if ( walkable( diagonals[ index ] ) === true
                    && walkable( [ diagonals[ index ][ 0 ], y ] ) === true
                    && walkable( [ x, diagonals[ index ][ 1 ] ] ) === true ) {

                        neighbor = getNode( diagonals[ index ] ) ;

                        // if neighbor is not closed then store it
                        if ( neighbor.closed === false ) {

                            neighbors.push( neighbor ) ;
                        }
                    }
                }

                // return all valid neighbors
                return ( neighbors ) ;
            } ;

            setNode = function setNode( parent, position ) {

                var node ;

                // create a new node
                node = {

                    'closed': false,
                    'cost': 0,
                    'heuristic': 0,
                    'opened': false,
                    'parent': parent,

                    'position': [

                        position[ 0 ],
                        position[ 1 ]
                    ]
                } ;

                // return created node
                return ( node ) ;
            } ;

            updateHeap = function updateHeap( newNode ) {

                var currentNode ;
                var index ;
                var targetIndex ;

                // define default position of the new node into open nodes heap
                targetIndex = 0 ;

                // indexing of the new node into open nodes heap
                for ( index = openHeap.length - 1 ; index >= 0 ; index -= 1 ) {

                    currentNode = openHeap[ index ] ;

                    // if new node's cost is lesser than current node's cost then new node's index is after current node's index
                    if ( currentNode.cost + currentNode.heuristic > newNode.cost + newNode.heuristic
                    || currentNode.cost + currentNode.heuristic === newNode.cost + newNode.heuristic
                    && currentNode.heuristic >= newNode.heuristic ) {

                        targetIndex = index + 1 ;

                        break ;
                    }
                }

                // insert new node into open node heap
                openHeap.splice( targetIndex, 0, newNode ) ;
            } ;

            walkable = function walkable( position ) {

                var x ;
                var y ;

                x = position[ 0 ] ;
                y = position[ 1 ] ;

                // define if position exists in grid and if position is walkable
                return ( typeof ( world.grid[ y ] ) !== 'undefined'
                && typeof ( world.grid[ y ][ x ] ) !== 'undefined'
                && world.grid[ y ][ x ] !== 0 ) ;
            } ;

            // initialize this instance
            initialize( heuristic ) ;
        } ;

        return ( Astar ) ;
    }
) ;
